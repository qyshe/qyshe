@extends('layouts.main')

@section('title')
      Subjects
@stop

@section('content')
{{ Form::open(array('url' => 'subjects')) }}
      <div class="row">
            <div class="col-sm-12 text-center">
                  <h1>Subjects</h1>
            </div>
      </div>
      <hr />
      @for($year=1;$year<=3;$year++)
      <div class="row">
            <div class="col-sm-12">
                  <h3>Year {{$year}}</h3>
            </div>
            @for($season=1; $season<=2; $season++)
            <div class="col-sm-6">
                  <div class="panel-group">
                        <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="heading{{$year}}{{$season}}">
                                    <h3 class="panel-title text-center">
                                          <a role="button" data-toggle="collapse" href="#collapse{{$year}}{{$season}}" aria-expanded="{{$season==1?'true':'false'}}" aria-controls="collapse{{$year}}{{$season}}">
                                                Season {{ $season }}
                                          </a>
                                    </h3>
                              </div>
                              <div id="collapse{{$year}}{{$season}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{$year}}{{$season}}">
                                    <div class="panel-body">
                                          <div class="row table-responsive">
                                                <table class="table table-striped table-hover table-condensed">
                                                      <thead>
                                                            <tr>
                                                                  <th>Subject</th>
                                                                  <th>Teacher</th>
                                                                  <th>Credits</th>
                                                                  <th class="text-center">Attend</th>
                                                            </tr>
                                                      </thead>
                                                      <tbody>
                                                            <?php $Subjects = $SubjectsModel::whereRaw('year = ? and season = ?', array($year, $season))->get(); ?>
                                                            @forelse($Subjects as $subject)
                                                            <tr>
                                                                  <td>
                                                                        {{ $subject->name }}
                                                                  </td>
                                                                  <td>
                                                                        <ul>
                                                                              <?php
                                                                                    $teachers = DB::table('subjects')
                                                                                          ->join('rel_teach', 'rel_teach.id_subject', '=', 'subjects.id')
                                                                                          ->join('users', 'rel_teach.id_user', '=', 'users.id')
                                                                                          ->select('users.name')
                                                                                          ->where(array('rel_teach.id_subject' => $subject->id))
                                                                                          ->get();
                                                                              ?>
                                                                                    @forelse ($teachers as $teacher)
                                                                                          {{ '<li>' . $teacher->name . '</li>' }}
                                                                                    @empty
                                                                                          {{ '<li class="text-danger">' . 'No teacher' . '<li>' }}
                                                                                    @endforelse
                                                                        </ul>
                                                                  </td>
                                                                  <td>
                                                                        {{ $subject->credits }}
                                                                  </td>
                                                                  <td class="text-center">
                                                                        <?php $attending = RelStudent::where(array('id_subject' => $subject->id, 'id_user' => $user))->exists() ?>
                                                                        {{ Form::checkbox($subject->id, true, $attending) }}
                                                                  </td>
                                                            </tr>
                                                            @empty
                                                                  <tr><td colspan="4" class="text-center"><span class="text-danger">No subjects available this season</td></tr></span>
                                                            @endforelse
                                                      </tbody>
                                                </table>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            @endfor
      </div>
      @endfor
      <div class="row">
            <div class="col-sm-12 text-right">
                  {{ Form::submit('Update', ['class' => 'btn btn-primary btn-md']) }}
            </div>
      </div>
{{ Form:: close() }}
@stop
