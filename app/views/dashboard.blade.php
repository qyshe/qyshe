@extends('layouts.main')

@section('title')
      Dashboard
@stop

@section('content')
      <div class="row">
            <div class="col-sm-12 text-center">
                  <h1>Dashboard</h1>
            </div>
      </div>
      <div class="row">

            @foreach ($events as $event)
                  <div class="col-md-3">
                        <p>This is event nr  {{ $event->id }}</p><br>
                        <b>{{ $event->title}}</b>
                        {{ $event->desc }}<br>
                        {{ $event->date}}
                        {{ $event->place }}
                  </div>
            @endforeach

      </div>

@stop
