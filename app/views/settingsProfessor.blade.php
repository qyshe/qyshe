@extends('layouts.main')

@section('title')
      Settings
@stop

@section('content')
<div style="padding-top:25px;">
    @foreach($theUser as $leUser)
            {{Form::open(array('url'=>'settings'))}}
                <h2 style="text-align: center;">Welcome {{$leUser->name}}</h2>
                <div class="row">
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>Name:
                            {{Form::text('name', $leUser->name, array('class'=>'form-control'))}}
                        </label>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>Email:
                            {{Form::text('email', $leUser->email, array('class'=>'form-control'))}}
                        </label>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>New Password:
                            {{Form::password('password', array('class'=>'form-control'))}}
                        </label>
                    </div>
                </div> 
                <div class="row" style="padding-top:30px;"> 
                    <h4>Choose the subject/s you teach.</h4>
                    @foreach($joined as $join)
                        <div class="col-xs-3">
                            <label>
                                @if($join->id_user == 2)
                                    {{Form::checkbox($join->id, true, true)}}
                                    {{$join->name}}
                                @else
                                    {{Form::checkbox($join->id, true, false)}}
                                    {{$join->name}}
                                @endif
                            </label>
                        </div>
                    @endforeach
                </div>
                <div class="row" style="text-align: center;">
                    {{Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            {{Form::close()}}
        @endforeach
</div>
@stop






