@extends('layouts.main')

@section('title')
      Settings
@stop

@section('content')
<div style="padding-top:25px;">
    @foreach($theUser as $leUser)
            {{Form::open(array('url'=>'settingsStudents'))}}
                <h2 style="text-align: center;">Welcome {{$leUser->name}}</h2>
                <div class="row">
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>Name:
                            {{Form::text('name', '', array('class'=>'form-control', 'placeholder'=>$leUser->name))}}
                        </label>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>Email:
                            {{Form::text('email', '', array('class'=>'form-control', 'placeholder'=>$leUser->email))}}
                        </label>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4">
                        <label>New Password:
                            {{Form::password('password', array('class'=>'form-control'))}}
                        </label>
                    </div>
                </div>
                <!--<div class="row" style="padding-top:30px;">
                    <h4>Choose the subject/s you teach.</h4>

                    @foreach($theSubjects as $theSubject)
                        <?php $attending = false; ?>
                        @foreach($theUser as $leUser)
                            @forelse($yourSubjects as $yourSubject)
                                @if($leUser->id == $yourSubject->id_user && $yourSubject->id_subject == $theSubject->id)
                                   <div class="col-xs-3">
                                        <label>
                                                {{Form::checkbox('agree', true, false, ['checked'=>'true'])}}
                                                {{$theSubject->name}}
                                            </label>
                                        </div>
                                    @else
                                        <div class="col-xs-3">
                                            <label>
                                                {{Form::checkbox('agree', true, false, ['class'=>'field'])}}
                                                {{$theSubject->name}}
                                            </label>
                                        </div>
                                     @endif
                            @empty
                            @endforelse
                        @endforeach
                        <div class="col-xs-3">
                            <label>
                                {{Form::checkbox('agree', 1, null, ['class'=>'field'])}}
                                {{$theSubject->name}}
                            </label>
                        </div>
                    @endforeach
                </div>-->
                <div class="row" style="text-align: center;">
                    {{Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            {{Form::close()}}
        @endforeach
</div>
@stop
