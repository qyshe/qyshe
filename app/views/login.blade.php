@extends('layouts.default')
@section('body')


{{ Form::open(array('url' => 'login', 'method' => 'post')) }}
    <p>
    <b>
    {{ $errors->first('name') }}
    {{ $errors->first('password') }}
    </b>
    </p>

{{Form::label('name','Name')}}
{{Form::text('name', null,array('class' => 'form-control'))}}
{{Form::label('password','Password')}}
{{Form::password('password',array('class' => 'form-control'))}}
<input type="checkbox" name="remember"  id="remember">
			<label for="remember">
				Remember me</label>
{{Form::submit('Login', array('class' => 'btn btn-primary'))}}
{{ Form::close() }}

@stop