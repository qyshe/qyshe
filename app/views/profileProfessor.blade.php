@extends('layouts.main')

@section('title')
      Profile
@stop

@section('content')
    <div style="padding-top:25px;">
        @foreach($theUser as $leUser)
            <h2>Welcome {{$leUser->name}}</h2>
            <table class="table table-responsive" style="margin-bottom: 20px;">
                <tr>
                    <td>Name</td>
                    <td>{{$leUser->name}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{$leUser->email}}</td>
                </tr>
            </table>
            <table class="table table-responsive">
                <thead>
                    <tr><th>Subjects</th><th>Credits</th></tr>
                </thead> 
                <tbody>
                    @foreach($yourSubjects as $yourSubject)
                        @foreach($theSubjects as $leSubject)
                            @if($leUser->id == $yourSubject->id_user)
                                @if ($yourSubject->id_subject == $leSubject->id)
                                    <tr>
                                        <td>{{$leSubject->name}}</td>
                                        <td>{{$leSubject->credits}}</td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
@stop