<html>
      <head>
            @section('head')
            <title>
                        Students Portal - @yield('title', 'Main')
            </title>
            <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
            <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
            <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
            <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
            @show
      </head>
      </head>
      <body>
            <div id="wrapper">
                  @section('header')
                  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                              </button>
                        </div>
                        <ul class="nav navbar-right top-nav">
                              <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Profile <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                          <li>
                                               <a href="/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                                          </li>
                                          <li>
                                                <a href="/settings"><i class="fa fa-fw fa-gear"></i> Settings</a>
                                          </li>
                                          <li class="divider"></li>
                                          <li>
                                          <a href="{{ URL::to('logout') }}"><i class="fa fa-fw fa-power-off"></i>Logout</a>
                                              <!--  <a href="#/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a> -->
                                          </li>
                                    </ul>
                              </li>
                        </ul>
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                              <ul class="nav navbar-nav side-nav">
                                    <li>
                                          <a href="/dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                                    </li>

                                    <li>
                                          <a href="/marks"><i class="fa fa-fw fa-table"></i> Marks</a>
                                    </li>
                                    <li>
                                          <a href="/subjects"><i class="fa fa-fw fa-edit"></i> Subjects</a>
                                    </li>
                                    <li>
                                          <a href="/marksProfessor"><i class="fa fa-fw fa-table"></i> Marks Professor</a>
                                    </li>
                              </ul>
                        </div>
                  </nav>
                  @show
                  <div class="page-wrapper">
                        <div class="container-fluid">
                              @yield('content')
                        </div>
                  </div>
            </div>
      </body>
</html>
