@extends('layouts.default')

@section('title')
      Log Out
@stop

@section('content')
      <div class="row">
            <div class="col-sm-12 text-center">
                  <h1>Log out</h1>
            </div>
      </div>
      <h2>Log out</h2>

     <p>You successfully logged out. <a href="{{URL::to('/login')}}">Login ? </a></p>

@stop
