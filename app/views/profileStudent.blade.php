@extends('layouts.main')

@section('title')
      Students
@stop

@section('content')
<div style="padding-top:25px;">
   @foreach($theUser as $sUser)
        <h2>Welcome {{$sUser->name}}</h2>
        <table class="table table-responsive" style="margin-bottom: 20px;">
           <tr>
                <td>Name</td>
                <td>{{$sUser->name}}</td>
           </tr>
           <tr>
                <td>Email</td>
                <td>{{$sUser->email}}</td>
           </tr>
        </table>
        <table class="table table-responsive">
           <thead>
                <tr><th>Subject</th><th>Professor</th><th>Mark</th><th>Credits</th></tr>
           </thead>
           <tbody>
                @foreach($yourSubjects as $yourSubject)
                    @foreach($theSubjects as $leSubject)
                        @foreach($yourTeach as $leTeach)
                           @if($leUser->id == $yourSubject->id_user)
                              @if ($yourSubject->id_subject == $leSubject->id)
                                 @if ($yourTeach->id_subject == $leSubject->id_subject)
                                    <tr>
                                        <td>{{$leSubject->name}}</td>
                                        <td>{{$leTeach->name}}</td>
                                        <td>{{$yourSubject->mark}}</td>
                                        <td>{{$leSubject->credits}}</td>
                                    </tr>
                                 @endif
                              @endif
                           @endif
                        @endforeach
                     @endforeach
                  @endforeach
           </tbody>
        </table>
   @endforeach
</div>
@stop
