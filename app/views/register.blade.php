@extends('layouts.default')
@section('body')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h2>Register here</h2>
        {{ Form::open(array('url' => 'register', 'method' => 'post')) }}
        <p><b>
            {{ $errors->first('name') }}
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
            </b>
        </p>
        <div class="form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name', null,array('class' => 'form-control'))}}
        </div>

        <div class="form-group">
            {{Form::label('email','Email')}}
            {{Form::text('email', null,array('class' => 'form-control'))}}
        </div>
        <div class="form-group">
            {{Form::label('password','Password')}}
            {{Form::password('password',array('class' => 'form-control'))}}
        </div>
        <div class="form-group">
                    {{Form::label('password_confirmation','Password Confirm')}}
                    {{Form::password('password_confirmation',array('class' => 'form-control'))}}
        </div>
        <div class="form-group">
         {{ Form::select('role', array(0 => 'Administrator', '1' => 'Student',2 => 'Profesor'))}}
        </div>
        {{Form::submit('Register', array('class' => 'btn btn-primary'))}}
        {{ Form::close() }}
    </div>
</div>
@stop
