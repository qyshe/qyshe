@extends('layouts.main')

@section('title')
    Marks
@stop

@section('content')
    <div class="container-fluid">
        <div class="col-xs-12 text-center">
            <h1>Marks</h1>
        </div>
        <hr><hr><hr>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach($theSubjects as $theSubject)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$theSubject->name}}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$theSubject->name}}" aria-expanded="true" aria-controls="collapse{{$theSubject->name}}">
                            {{$theSubject->name}}
                        </a>
                    </h4>
                </div>
                <div id="{{$theSubject->name}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$theSubject->name}}">
                    {{Form::open(array('url'=>'marksProfessor'))}}
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <thead>
                                <th>Student</th>
                                <th>Mark</th>
                            </thead>
                            <tbody>
                                @foreach($students as $student)
                                    @if($student->id_subject == $theSubject->id)
                                        <tr>
                                            <td>{{$student->name}}</td>
                                            <td width="15%">
                                                {{Form::text($student->rsid, $student->mark, array('class'=>'form-control'))}}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <div class="container-fluid pull-right" style="padding-bottom: 15px;">
                            {{Form::submit('Save', array('class'=>'btn btn-success'))}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            @endforeach
        </div>
    </div>
@stop