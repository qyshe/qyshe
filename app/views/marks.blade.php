@extends('layouts.main')

@section('title')
      Marks
@stop

@section('content')
      <div class="row">
            <div class="col-sm-12 text-center">
                  <h1>Marks</h1>
            </div>
      </div>
      <hr>
      <div class="row">
            <div class="col-sm-8">
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <?php $num = 0; $sum = 0; ?>
                  @for($year=1;$year<=3;$year++)
                        <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="heading{{$year}}">
                                    <h3 class="panel-title text-center">
                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$year}}" aria-expanded="{{$year==1?'true':'false'}}" aria-controls="collapse{{$year}}">
                                                Year {{ $year }}
                                          </a>
                                    </h3>
                              </div>
                              <div id="collapse{{$year}}" class="panel-collapse collapse {{$year==1?'in':''}}" role="tabpanel" aria-labelledby="heading{{$year}}">
                                    <div class="panel-body">
                                          <div class="row table-responsive">
                                                <table class="table table-striped table-hover table-condensed">
                                                      <thead>
                                                            <tr>
                                                                  <th>Subject</th>
                                                                  <th>Teacher</th>
                                                                  <th>Credits</th>
                                                                  <th class="text-center">Your mark</th>
                                                            </tr>
                                                      </thead>
                                                      <tbody>
                                                            <?php $Subjects = DB::table('rel_student')
                                                                                    ->join('subjects', 'rel_student.id_subject', '=', 'subjects.id')
                                                                                    ->where(array('rel_student.id_user' => $user_id, 'subjects.year' => $year))
                                                                                    ->get(); ?>
                                                            @forelse($Subjects as $subject)
                                                            <tr>
                                                                  <td>
                                                                        {{ $subject->name }}
                                                                  </td>
                                                                  <td>
                                                                        <ul>
                                                                              <?php $teachers = DB::table('subjects')
                                                                                    ->join('rel_teach', 'rel_teach.id_subject', '=', 'subjects.id')
                                                                                    ->join('users', 'users.id', '=', 'rel_teach.id_user')
                                                                                    ->select('users.name')
                                                                                    ->where(array('rel_teach.id_subject' => $subject->id))
                                                                                    ->get(); ?>
                                                                              @forelse($teachers as $teacher)
                                                                                    <li>{{ $teacher->name }}</li>
                                                                              @empty
                                                                                    <li class="text-danger"> No teacher </li>
                                                                              @endforelse
                                                                        </ul>
                                                                 </td>
                                                                  <td>
                                                                        {{ $subject->credits }}
                                                                  </td>
                                                                  <td class="text-center">
                                                                        <?php $mark = DB::table('rel_student')
                                                                                          ->select(array('rel_student.mark'))
                                                                                          ->where(array('id_subject' => $subject->id, 'id_user' => $user_id))
                                                                                          ->first(); ?>

                                                                              @if(!is_null($mark->mark))
                                                                                    <span>{{ $mark->mark }}</span>
                                                                                    <?php $sum += $subject->credits ?>
                                                                                    <?php $num += $subject->credits * $mark->mark ?>
                                                                              @else
                                                                                    <span class="text-warning">?</span>
                                                                              @endif


                                                                  </td>
                                                            </tr>
                                                            @empty
                                                                  <tr><td colspan="4" class="text-center"><span class="text-danger">No subjects available this year</td></tr></span>
                                                            @endforelse
                                                      </tbody>
                                                </table>
                                          </div>
                                    </div>
                              </div>
                        </div>
                        @endfor
                  </div>
            </div>
            <div class="col-sm-4">
                  <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                              <tr>
                                    <th>Your average:</th>
                              </tr>
                        </thead>
                        <tbody>
                              <tr>
                                    <td>
                                          {{ $sum!=0 && $num!=0?(round($num/$sum, 2)):'No exams taken' }}
                                    </td>
                              </tr>
                        </tbody>
                  </table>
            </div>
      </div>
@stop
