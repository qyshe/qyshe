<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(array('before' => 'auth'), function(){

	Route::get('/marks', 'MarksController@showMarks');

	Route::get('/subjects', 'SubjectsController@showSubjects');
	Route::post('/subjects', 'SubjectsController@saveSubjects');

	Route::get('/students', 'StudentsController@showStudent');

	Route::get('/settingsStudent', 'StudentsController@showSettings');
	Route::post('/settingsStudent', 'StudentsController@redirectProfile');

	Route::get('logout', array('uses' => 'HomeController@doLogout'));

	Route::get('/marksProfessor', 'MarksProfessorController@showMarks');
	Route::post('/marksProfessor', 'MarksProfessorController@saveMarks');

	Route::get('dashboard','DashboardController@showDashboard');

	Route::get('/profile', 'ProfileController@showProfile');

	Route::get('/settings', 'ProfileController@showSettings');
	Route::post('/settings', 'ProfileController@redirectProfile');

});

Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));

Route::get('/register','RegisterController@showRegister');
Route::post('/register','RegisterController@doRegister');
