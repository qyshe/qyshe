<?php

      class StudentsController extends BaseController{

            public function showStudent(){
               $theUser = DB::table('users')->where('id', '=', 3)->get();
               $theSubjects = DB::table('subjects')->get();
               $yourSubjects = DB::table('rel_student')->where('id_user', '=', 3)->get();
               $yourTeach = DB::table('rel_teach')->get();
               return View::make('profileStudent', array('theUser'=> $theUser, 'yourSubjects' => $yourSubjects, 'theSubjects' => $theSubjects, 'yourTeach' => $yourTeach));
            }

            public function editStudent(){
               $theUser = User::where('id', '=', 3)->get();
               $theUser -> name = Input::get('name');
               save();
               return View::make('students');
            }

            public function  showSettings(){
               $theUser = DB::table('users')->where('id', '=', 3)->get();
               $theSubjects = DB::table('subjects')->get();
               $yourSubjects = DB::table('rel_student')->where('id_user', '=', 3)->get();
               return View::make('settingsStudent', array('theUser'=> $theUser, 'yourSubjects' => $yourSubjects, 'theSubjects' => $theSubjects));
            }

            public function redirectProfile(){
               $theUser = DB::table('users')->where('id', '=', 3)->get();
               if (Input::get('name'))
                  {
                     $theUser = DB::table('users')->where('id', '=', 3)->update(['name' =>Input::get('name')]);
                  }
               if (Input::get('email'))
                  {
                     $theUser = DB::table('users')->where('id', '=', 3)->update(['email' =>Input::get('email')]);
                  }
               if (Input::get('password'))
                  {
                     $theUser = DB::table('users')->where('id', '=', 3)->update(['password' =>Input::get('password')]);
                  }
               return Redirect::to('students');
            }

      }

 ?>
