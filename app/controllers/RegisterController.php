<?php

class RegisterController extends BaseController
{
    public function showRegister()
      {
             return View::make('register');
      }
    
    public function doRegister()
    {
        $data = Input::only(['name','email','password', 'password_confirmation']);
        $validator = Validator::make(
            $data,
            [
                'name' => 'required|min:5',
                'email' => 'required|email|min:3',
                'password' => 'required|min:3|confirmed',
                'password_confirmation'=> 'required|min:3'
            ]
        );

        if($validator->fails())
        {
            return Redirect::to('/register')->withErrors($validator)->withInput();
        }
        else
        {
            $user = new User;
            $user->email = Input::get('email');
            $user->name = Input::get('name');
            $user->password = Hash::make(Input::get('password'));
            $user->role = Input::get('role');
            $user->save();
            $theEmail = Input::get('email');
            return View::make('thanks')->with('theEmail', $theEmail);
        }
    }
}