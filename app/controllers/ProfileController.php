<?php

    class ProfileController extends BaseController{
        /*  This method is called to retrieve data from the database to create the query.
            Also this method is used to return the template.*/
        public function showProfile(){
            $theUser = DB::table('users')->where('id', '=', Auth::id())->get();
            $theSubjects = DB::table('subjects')->get();
            $yourSubjects = DB::table('rel_teach')->where('id_user', '=', Auth::id())->get();
            return View::make('profileProfessor', array('theUser'=> $theUser, 'yourSubjects' => $yourSubjects, 'theSubjects' => $theSubjects));
        }

        //  This method is used to show the data the user has provided
        public function showSettings(){
            $theUser = DB::table('users')->where('id', '=', Auth::id())->get();
            $theSubjects = DB::table('subjects')->get();
            $yourSubjects = DB::table('rel_teach')->where('id_user', '=', Auth::id())->get();
            $joined = DB::table('subjects')
                ->leftJoin('rel_teach', function($join){
                    $join->on('subjects.id', '=', 'rel_teach.id_subject')
                         ->where('rel_teach.id_user', '=', Auth::id());
                })
                ->select('subjects.id', 'subjects.name', 'rel_teach.id_user', 'rel_teach.id_subject')
                ->get();
            return View::make('settingsProfessor', array('theUser'=>$theUser, 'theSubjects'=>$theSubjects, 'yourSubjects'=>$yourSubjects, 'joined'=>$joined));
        }
        
        /*  This method is used to redirect to the user in the profile
            page after saving his new information. The update of information occurs
            only if the fileds are not null or have empty strings.*/
        public function redirectProfile(){
            $theSubjects = DB::table('subjects')->get();
            $theUser = DB::table('users')->where('id', '=', Auth::id())->get();
            if (Input::get('name'))
                {DB::table('users')->where('id', Auth::id())->update(['name' => Input::get('name')]);}
            if (Input::get('email'))
                {DB::table('users')->where('id', Auth::id())->update(['email' => Input::get('email')]);}
            if (Input::get('password'))
                {DB::table('users')->where('id', Auth::id())->update(['password' => Input::get('password')]);}
            for($i=1; $i<=count($theSubjects); $i++){
                if(Input::get($i)){
                    $exists = DB::table('rel_teach')->where(array('id_subject'=> $i,'id_user'=> Auth::id()))->count();
                    if($exists==0)
                        DB::table('rel_teach')->insert(array('id_user'=>Auth::id(), 'id_subject'=>$i));
                }
                else
                    DB::table('rel_teach')->where(array('id_subject'=> $i,'id_user'=> Auth::id()))->delete();
            }    
            return Redirect::to('profile');
        }
}
