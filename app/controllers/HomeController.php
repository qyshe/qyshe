<?php

class HomeController extends BaseController
{
	public function showWelcome()
	{
		return View::make('hello');
	}


    public function doLogout()
    {
        if(Auth::check()){
            Auth::logout();
        }
        //return Redirect::route('login');
        //Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }

    public function showLogin()
    {
        // show the form
        return View::make('login');
    }

    public function doLogin()

    {
                // validate the info, create rules for the inputs
        $rules = array(
            'name'    => 'required|alphaNum|min:3',
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
                        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else
        {

            $userdata = array('name'     => Input::get('name'), 'password'  => Input::get('password'));

            // attempt to do the login
            if (Auth::attempt($userdata, true))
            {
                return Redirect::to('dashboard');
            }
            else
            {
                // validation not successful, send back to form
                return Redirect::to('login');
            }

        }
    }

}
