<?php

      class SubjectsController extends BaseController{

            public function showSubjects(){
                  $SubjectsModel = 'Subjects';
                  return View::make('subjects', array('SubjectsModel' => $SubjectsModel, 'user' => Auth::id()));
            }

            public function saveSubjects(){
                  $user = Auth::id();
                  $theSubjects = Subjects::all();
                  foreach ($theSubjects as $key)
                  {
                        $result = DB::table('rel_student')->where(array('id_user' => $user, 'id_subject' => $key->id))->first();
                        $input = Input::get($key->id);
                        if(is_null($result))
                        {
                              if($input) {
                                    $new_subject = new RelStudent;
                                    $new_subject->id_user = $user;
                                    $new_subject->id_subject = $key->id;
                                    $new_subject->save();
                              }
                        }
                        else
                        {
                              if(!$input) {
                                    RelStudent::where(array('id_user' => $user, 'id_subject' => $key->id))->delete();
                              }
                        }
                  }
                  $SubjectsModel = 'Subjects';
                  return Redirect::to('subjects');
            }

      }

 ?>
