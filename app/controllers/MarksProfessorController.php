<?php

    class MarksProfessorController extends BaseController{
        
        public function showMarks(){
            $leSubjects = DB::table('subjects')->get();
            $index = array();
            $k = 0;
            for ($i=1; $i<=count($leSubjects); $i++){
                $yourSubjects = DB::table('rel_teach')
                    ->where(array('id_user'=> Auth::id(), 'id_subject'=>$i))
                    ->count();
                if($yourSubjects){
                    $index[$k]=$i;
                    $k +=1;
                }
            }
            $theSubjects = DB::table('subjects')
                ->whereIn('id', array_values($index))
                ->get();
            
            $students = DB::table('users')
                ->join('rel_student', 'users.id', '=', 'rel_student.id_user')
                ->join('subjects', 'subjects.id', '=', 'rel_student.id_subject')
                ->select('subjects.name as Sname', 'users.name', 'rel_student.mark', 'rel_student.id as rsid', 'rel_student.id_user', 'rel_student.id_subject')
                ->whereIn('subjects.id', array_values($index))
                ->get();
            
            return View::make('marksProfessor', array('theSubjects'=> $theSubjects, 'students'=>$students));
        }
        
        public function saveMarks(){
           $leSubjects = DB::table('subjects')->get();
            $index = array();
            $k = 0;
            for ($i=1; $i<=count($leSubjects); $i++){
                $yourSubjects = DB::table('rel_teach')
                    ->where(array('id_user'=> Auth::id(), 'id_subject'=>$i))
                    ->count();
                if($yourSubjects){
                    $index[$k]=$i;
                    $k +=1;
                }
            }
            $theSubjects = DB::table('subjects')
                ->whereIn('id', array_values($index))
                ->get();
            $students = DB::table('users')
                ->join('rel_student', 'users.id', '=', 'rel_student.id_user')
                ->join('subjects', 'subjects.id', '=', 'rel_student.id_subject')
                ->select('rel_student.id as rsid')
                ->whereIn('subjects.id', array_values($index))
                ->get();
            foreach($students as $student){
                if(Input::get($student->rsid)!=0)
                    DB::table('rel_student')
                    ->where('id', $student->rsid)
                    ->update(array('mark' => Input::get($student->rsid)));
            }
            return Redirect::to('profile');
        }
}
