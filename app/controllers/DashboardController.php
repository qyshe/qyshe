<?php

class DashboardController extends BaseController{

    public function showDashboard(){
        $events = Events::all();

        return   View::make('dashboard', array('events' => $events
           ));
    }

}

?>
