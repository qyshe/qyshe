<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';


   public function RelStudent(){

   	return $this-> hasMany('rel_student', 'id_user');
   }

   public function RelTeach(){

		return $this-> hasMany('rel_teach','id_user');
   }


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function rel_student()
	{
      	return $this->hasMany('RelStudent', 'id_user');
	}

	public function rel_teach()
	{
      	return $this->hasMany('RelTeach', 'id_user');
	}

}
