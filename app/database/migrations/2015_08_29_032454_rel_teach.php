<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelTeach extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::create('rel_teach', function($teach){
			$teach -> increments('id');
			$teach -> integer('id_subject')->unsigned();
			$teach -> foreign('id_subject')->references('id')->on('subjects');
			$teach -> integer('id_user')->unsigned();
			$teach -> foreign('id_user')->references('id')->on('users');
			$teach -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('rel_teach');
	}

}
