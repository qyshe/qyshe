<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelStudent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::create('rel_student', function($student){
			$student -> increments('id');
			$student -> integer('id_subject')->unsigned();
			$student -> foreign('id_subject')->references('id')->on('subjects');
			$student -> integer('id_user')->unsigned();
			$student -> foreign('id_user')->references('id')->on('users');
			$student -> integer('mark')->nullable()->default(null);
			$student -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('rel_student');
	}

}
