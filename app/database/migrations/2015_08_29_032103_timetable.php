<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timetable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::create('timetable', function($timetable) {
			$timetable -> increments('id');
			$timetable -> integer('id_day')->unsigned();
			$timetable -> foreign('id_day')->references('id')->on('days');
			$timetable -> integer('id_subject')->unsigned();
			$timetable -> foreign('id_subject')->references('id')->on('subjects');
			$timetable -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('timetable');
	}

}
