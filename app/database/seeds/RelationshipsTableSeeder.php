<?php

      class RelationshipsTableSeeder extends Seeder {

            public function run(){
                  $faker = Faker\Factory::create();

                  RelTeach::truncate();

                  for($i=1; $i<=5; $i++)
                  {
                        $subject = RelTeach::create(array(
                              'id_subject' => rand(1,10),
                              'id_user' => $i+1
                        ));
                  }

                  RelStudent::truncate();

                  for($i=1; $i<=5; $i++)
                  {
                        $subject = RelStudent::create(array(
                              'id_subject' => rand(1,10),
                              'id_user' => $i+6,
                              'mark' => rand(4,10)
                        ));
                  }
            }

      }
