<?php

      class EventsTableSeeder extends Seeder {

            public function run(){
                  $faker = Faker\Factory::create();

                  Events::truncate();

                  for($i=0; $i<10; $i++)
                  {
                        $event = Events::create(array(
                              'title' => $faker->word($nb = rand(2,3)),
                              'desc' => $faker->paragraph($nbSentences = rand(3,4)),
                              'date' => $faker->dateTime($max = 'now'),
                              'place' => $faker->word($nb = rand(1,2))
                        ));
                  }
            }

      }
