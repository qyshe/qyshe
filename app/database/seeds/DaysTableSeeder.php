<?php

      class DaysTableSeeder extends Seeder {

            public function run() {

                  Days::truncate();

                  $days = array('Monday','Tuesday','Wednesday','Thursday','Friday');

                  for($i=0; $i<5; $i++)
                  {
                        $day = Days::create(array(
                              'name' => $days[$i]
                        ));
                  }
                  
            }

      }
