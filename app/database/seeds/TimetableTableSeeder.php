<?php

      class TimetableTableSeeder extends Seeder {

            public function run() {

                  Timetable::truncate();

                  for($i=1; $i<=5; $i++)
                  {
                        $timetable = Timetable::create(array(
                              'id_day' => $i,
                              'id_subject' => $i
                        ));
                  }

            }

      }
