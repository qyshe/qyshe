<?php

      class UsersTableSeeder extends Seeder {

            public function run(){
                  $faker = Faker\Factory::create();

                  User::truncate();

                  $user = User::create(array(
                        'name' => 'admin',
                        'email' => 'admin@admin.admin',
                        'password' => '$2a$04$K9C.OYalR5uk8IaBcyJddOJE.IXbJ6LakQg2d.hzw6D.7Zq.ToKpu',
                        'role' => 1
                  ));

                  for($i=1; $i<=5; $i++)
                  {
                        $user = User::create(array(
                              'name' => 'teacher' . $i,
                              'email' => $faker->email,
                              'password' => '$2a$04$K9C.OYalR5uk8IaBcyJddOJE.IXbJ6LakQg2d.hzw6D.7Zq.ToKpu',
                              'role' => 2
                        ));
                  }
                  for($i=1; $i<=5; $i++)
                  {
                        $user = User::create(array(
                              'name' => 'student' . $i,
                              'email' => $faker->email,
                              'password' => '$2a$04$K9C.OYalR5uk8IaBcyJddOJE.IXbJ6LakQg2d.hzw6D.7Zq.ToKpu',
                              'role' => 3
                        ));
                  }
            }

      }
