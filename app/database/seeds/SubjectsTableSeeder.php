<?php

      class SubjectsTableSeeder extends Seeder {

            public function run(){
                  $faker = Faker\Factory::create();

                  Subjects::truncate();

                  for($i=0; $i<10; $i++)
                  {
                        $subject = Subjects::create(array(
                              'name' => $faker->word,
                              'credits' => rand(6,12),
                              'year' => rand(1,3),
                              'season' => rand(1,2)
                        ));
                  }
            }

      }
